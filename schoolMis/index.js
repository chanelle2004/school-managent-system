require("./model/mongodb");
const express=require("express")
const app=express();
const schoolsController=require('./controllers/schoolsControllers');
const studentControllers=require('./controllers/studentControllers');
const userControllers=require('./controllers/usersController');
const config = require('config')
const admin=require('./middlewares/admin');
const authMiddleware = require('./middlewares/auth')
const bodyparser = require('body-parser');


app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

if(!config.get("jwtPrivateKey")){
    console.log('JWT PRIVATE KEY IS NOT DEFINED')
    process.exit(1)
}

app.get('/',(req,res)=>{
    res.send("Welcome to studentMIS");
})

app.use('/api/schools',authMiddleware,schoolsController)
app.use('/api/students',authMiddleware,studentControllers)
app.use('/api/users',userControllers)

let port=process.env.PORT||4000
app.listen(port,()=>console.log(`Server is running on port ${port}`))