const mongoose=require("mongoose");
const Joi = require('joi')
const bcrypt = require('bcrypt')
const jwt  =  require('jsonwebtoken')
const config = require('config')

let userSchema=new mongoose.Schema({
    name:{
        type:String,
        maxlength: 255,
        minlength: 3,
        required:true
    },
    schoolId:{
        type:String,
        maxlength: 100,
        minlength: 6,
        required:true
    },
    email:{
        type:String,
        unique:true,
        maxlength: 30,
        minlength: 10,
        required:true
    },
    password:{
        type:String,
        maxlength: 100,
        minlength: 20,
        required:true
    },
    isAdmin:{
        type:Boolean,
        default:false,
        required:true
    }
});    

userSchema.methods.generateAuthToken = function(){
    const token  =jwt.sign({_id:this._id,name:this.name,schoolId:this.schoolId,email:this.email,isAdmin:this.isAdmin},config.get('jwtPrivateKey'))
    return token
}
const User = mongoose.model('user', userSchema);
function validateUser(user){
    const schema = {
        name:Joi.string().max(255).min(3).required(),
        schoolId:Joi.string().max(255).min(3).required(),
        email: Joi.string().max(255).min(3).required().email(),
        password:Joi.string().max(255).min(3).required(),
        isAdmin:Joi.required()
    }
    return Joi.validate(user,schema)
}
module.exports.User = User
module.exports.validate= validateUser