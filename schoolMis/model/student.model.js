const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose = require('mongoose');

let studentSchema = new mongoose.Schema({
    names: {
        type: String,
        required: true
    },
    schoolId: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    gender: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    }
});

const Student = mongoose.model('Student', studentSchema);
function validateStudent(student){
    const schema = {
        names:Joi.string().max(255).min(3).required(),
        schoolId: Joi.objectId().required(),
        email:Joi.required(),
        gender: Joi.string().required(),
        age:Joi.number().required()
    }
    return Joi.validate(student,schema);
}
module.exports.Student = Student;
module.exports.validateStudent= validateStudent;