const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)
const mongoose=require('mongoose');

let schoolSchem=new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    district:{
        type:String,
        required:true
    },
    sector:{
        type:String,
        required:true
    },
    email:{
        type:String,
        unique:true,
        required:true
    }
});

const School=mongoose.model('School',schoolSchem);
function ValidateSchool(school){
      const shema={
        name:Joi.string().required(),
        district:Joi.string().require(),
        sector:Joi.string().required(),
        email:Joi.string().required()
      }
        return Joi.validate(school,schema)
}

module.exports.School=School;
module.exports.ValidateSchool=ValidateSchool;