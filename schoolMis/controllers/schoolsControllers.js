const mongoos=require("mongoose");
const express=require("express");
const _=require("lodash");
const router=express.Router();
const {School,ValidateSchool}=require('../model/schools.model');
const admin=require('../middlewares/admin');
router.post('/',admin,(req,res)=>{
 insertNewSchool(req,res);
})


router.get('/',(req,res)=>{
    School.find()
    .then(s=>res.send(s))
    .catch(err=>res.send(err).send(400));
})

//get schools per sector
router.get('/sector/:sector',(req,res)=>{
    School.find({sector:req.params.sector})
     .then(sch=>res.send(sch))
     .catch(err=>res.send(err).status(404))
});

///get schools per sitrict 
router.get('/district/:district',(req,res)=>{
    School.find({district:req.params.district})
    .then(sch=>res.send(sch))
    .catch(err=>res.send(err).status(404))
});
function insertNewSchool(req,res){
    const {error}=ValidateSchool(req.body);
    if(error) return res.send(error.details[0].message).status(400)
    let school=new School(_.pick(req.body,["name","district","sector","email"]));
    school.save()
        .then(s=>res.send(s).status(201))
        .catch(err=>res.send(err).send(400));
}

router.delete('/:id',admin,(req, res) => {
    School.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
    });

router.put('/',admin,(req,res)=>{
    School.findByIdAndUpdate({_id:req.body._id})
       .then(schlupdated=>res.send(schlupdated))
       .catch(err=>res.send(err));
})
module.exports=router;