//Import the dependencies
const hashPassword = require('../utils/hash')
const _= require('lodash')
const express = require('express');
const bcrypt=require('bcrypt');
const {User,validate} =  require('../model/users.model')
const admin=require('../middlewares/admin')
//Creating a Router
var router = express.Router();

//get users
router.get('/',async (req,res)=>{
    const users = await User.find().sort({name:1});
    return res.send(users)
});
router.post('/',async (req,res) =>{
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    //check if the user exits in the db
    let user  = await User.findOne({email:req.body.email})
    if(user) return res.send('User already registered').status(400)


    user  =  new User(_.pick(req.body, ['name','schoolId','email','password','isAdmin']))
    const hashed = await hashPassword(user.password)
   user.password = hashed
   await user.save();
    return res.send(_.pick(user,['_id','name','schoolId','email','isAdmin'])).status(201)
});


//log in
router.post('/login',async (req,res) =>{
  let user  = await User.findOne({email:req.body.email})
  if(!user) return res.send('Invalid email or password').status(400)
  const validPassword = await bcrypt.compare(req.body.password,user.password)
  if(!validPassword) return res.send('invalid email or password').status(400)
 //const token  =jwt.sign({_id:user._id,name:user.name},config.get('jwtPrivateKey'))
  //return res.send(token);
  return res.send(user.generateAuthToken())
});
module.exports = router;