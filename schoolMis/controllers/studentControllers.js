const mongoos = require("mongoose");
const express = require("express");
const _=require("lodash");
const router = express.Router();
const {Student,validateStudent}=require('../model/student.model');
const {School,ValidateSchool}=require('../model/schools.model');
const admin=require('../middlewares/admin')
router.post('/',admin,(req, res) => {
    insertNewStudent(req, res);
})
router.get('/', (req, res) => {
    Student.find()
        .then(s => res.send(s))
        .catch(err => res.send(err).send(400));
})
router.put('/',admin,(req, res) => {
    updateIntoMongoDB(req, res);
})

function updateIntoMongoDB(req, res) {
    Student.findOneAndUpdate({ _id: req.body._id },
        req.body, { new: true })
        .then(course => res.send(course))
        .catch(err => res.send(err).status(400));
}
function insertNewStudent(req, res) {
    const {error} = validateStudent(req.body);
    if(error) return res.send(error.details[0].message).status(400)
    
    let checkid=School.find({_id:req.body.email})

    const student = new Student(_.pick(req.body,['names','schoolId','email','age','gender']));
    if(req.user.schoolId ==  req.body.schoolId){
        student.save()
        .then(s => res.send(s).status(201))
        .catch(err => res.send(err).send(400));
    }else{
        return res.send("You are not allowed to insert new student")
    }
}
router.delete('/:id',admin,(req, res) => {
    Student.findByIdAndRemove(req.params.id)
        .then(sch => res.send(sch))
        .catch(err => res.send(err).status(404));
});


//find students per school
router.get('/perschool/:id',(req,res)=>{
Student.find().countDocuments({_id:req.params.id})
.then(count => res.send(`Total=  ${count}`).status(200))
.catch(err => res.send(err).status(200))
})

router.get('/bygender/:gender',(req,res)=>{
    Student.find().
       countDocuments({gender:req.params.gender})
       .then(count => res.send(`Total=  ${count}`).status(200))
       .catch(err => res.send(err).status(200))
})

//get studets in the sector
router.get('/studSector/:sector',async(req,res)=>{
    let i = 0;
    let schoolFromSector = await School.find({sector:req.params.sector});
    console.log(schoolFromSector)
    let count=0;
      try{
          for(i = 0; i < schoolFromSector.length; i++){
            StudentsfromSector = await Student.find({schoolId:schoolFromSector[i]._id}).countDocuments();
          count +=StudentsfromSector;
        }
        return count.toString()
        }
        catch(err){
            console.log(err)
            return;
        }

})
module.exports = router;